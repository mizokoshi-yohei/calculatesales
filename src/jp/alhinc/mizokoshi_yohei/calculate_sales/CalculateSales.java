package jp.alhinc.mizokoshi_yohei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;

public class CalculateSales {

	public static void main(String[] args) {

		HashMap<String, String> storeMap = new HashMap<>(); //支店定義ファイル
		HashMap<String, Long> salesMap = new HashMap<>(); //売り上げファイル
		BufferedReader br = null;
		try { //try~catch (予期せぬエラー)
			try { //支店定義ファイル読み込み

				File file = new File(args[0], "branch.lst"); //branch.lst 支店定義ファイルの読み込み
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {
					
					String[] str = line.split(",");
					if (str.length != 2 || !line.matches("^[0-9]{3},.+$")) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						br.close();
					return;
					}
					storeMap.put(str[0], str[1]);
					
					
				}

			} catch (FileNotFoundException error) {
				System.out.println("支店定義ファイルが存在しません");

				return;
			}

			FilenameFilter filter = new FilenameFilter() { //数字８桁且つ、拡張子rcdのファイルを抽出
				public boolean accept(File file, String str) {
					return str.matches("^[0-9]{8}.rcd$");
				}
			};

			File[] list = new File(args[0]).listFiles(filter);
			for (int i = 0; i < list.length; i++) {
				FileReader fr2 = new FileReader(list[i]);
				br = new BufferedReader(fr2);

				String filename = list[i].getName().substring(0, 8); //ファイル名が歯抜けになっている場合のエラー処理
				int filenameInt = Integer.parseInt(filename);
				if (filenameInt - i != 1) {
					System.out.println("売り上げファイル名が連番になっていません");
					fr2.close();
					br.close();
					return;
				}

				String storeNumber = br.readLine();
				String sales = br.readLine();
				if (br.readLine() != null) { //売り上げファイルの中身が３行以上ある場合のエラー処理
					System.out.println(filename + ".rcd" + "のフォーマットが不正です");
					fr2.close();
					br.close();
					return;
				}

				long salesLong = Long.parseLong(sales);

				if (salesMap.containsKey(storeNumber)) { //支店コードに重複がある場合、合計する
					long salesLong2 = salesMap.get(storeNumber);
					salesLong = (salesLong2 + salesLong);
				}

				if (!storeMap.containsKey(storeNumber)) { //支店に該当がなかった場合のエラー処理
					System.out.println(filename + ".rcd" + "の支店コードが不正です");
					fr2.close();
					br.close();
					return;
				}

				long max = 9999999999L; //合計金額が１０桁を超えるときのエラー処理
				if (salesLong > max) {
					System.out.println(filename + ".rcd" + "の合計金額が10桁を超えました");
					fr2.close();
					br.close();
					return;
				}

				salesMap.put(storeNumber, salesLong);

			}

			File fileOut = new File(args[0], "branch.out"); //集計結果出力
			FileWriter fw = new FileWriter(fileOut);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for (HashMap.Entry<String, String> entry : storeMap.entrySet()) {
				if (salesMap.get(entry.getKey()) == null) {
					bw.write(entry.getKey() + "," + entry.getValue() + "," + 0);
					bw.close();
					return;
				}
				bw.write(entry.getKey() + "," + entry.getValue() + "," + salesMap.get(entry.getKey()));
				
				bw.newLine();
			}
			bw.close();

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
	}
}
